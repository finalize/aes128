/*
@file: main.cpp
@author: ZZH
@time: 2022-10-17 16:50:39
@info: aes算法实现
*/
#include <iostream>
#include "EnDecrypt.h"
#include "Mat.h"

using namespace std;
using namespace AES;

int main(const int argc, const char** argv)
{
    uint8_t data[5];
    uint8_t out[getIntegerMultiplesOf16(sizeof(data) + 1)];

    for (int i = 0;i < sizeof(data);i++)
        data[i] = i;

    cout << "data:";
    for (auto d : data)
        printf("%02X ", d);
    cout << endl;

    uint8_t key[4][4] = {
        0xEA, 0x04, 0x65, 0x85,
        0x83, 0x45, 0x5D, 0x96,
        0x5C, 0x33, 0x98, 0xB0,
        0xF0, 0x2D, 0xAD, 0xC5,
    };

    cout << "key:";
    for (int i = 0;i < 4;i++)
    {
        for (int j = 0;j < 4;j++)
            printf("%02X ", key[j][i]);//AES矩阵格式的顺序是先列后行
    }
    cout << endl;

    size_t encryptedLen = encrypt(out, data, key, sizeof(data));

    cout << "encrypted data(" << encryptedLen << "): ";
    for (auto d : out)
        printf("%02X ", d);
    cout << endl;

    size_t decryptedLen = decrypt(out, out, key, encryptedLen);

    cout << "decrypted data(" << decryptedLen << "): ";
    for (int i = 0; i < decryptedLen;i++)
        printf("%02X ", out[i]);
    cout << endl;

    return 0;
}
