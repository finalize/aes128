/*
@file: EnDecrypt.h
@author: ZZH
@time: 2022-10-20 15:09:59
@info: 加解密实现
*/
#pragma once
#include <cstdint>

namespace AES
{
    std::size_t encrypt(void* output, const void* data, const void* key, std::size_t len);
    
    std::size_t decrypt(void* output, const void* data, const void* key, std::size_t len);

    constexpr std::uint32_t getIntegerMultiplesOf16(std::uint32_t len) { return ((len >> 4) + (len % 16 > 0)) << 4; }
} // namespace AES
