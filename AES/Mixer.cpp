/*
@file: Mixer.cpp
@author: ZZH
@time: 2022-10-31 16:39:35
@info: 混淆器, 用于实现AES列混淆
*/
#include "Mat.h"

namespace AES
{
    uint8_t Mat_t::mix(uint8_t mix, uint8_t data)
    {
        uint8_t res = 0;
        while (mix)
        {
            if (mix & 0x01)
                res ^= data;

            mix >>= 1;

            if (data & 0x80)
            {
                data <<= 1;
                data ^= 0x1B;
            }
            else
            {
                data <<= 1;
            }
        }

        return res;
    }

    const uint8_t Mat_t::mixer[4][4] = {
        {0x02, 0x03, 0x01, 0x01},
        {0x01, 0x02, 0x03, 0x01},
        {0x01, 0x01, 0x02, 0x03},
        {0x03, 0x01, 0x01, 0x02}
    };

    const uint8_t Mat_t::imixer[4][4] = {
        {0x0E, 0x0B, 0x0D, 0x09},
        {0x09, 0x0E, 0x0B, 0x0D},
        {0x0D, 0x09, 0x0E, 0x0B},
        {0x0B, 0x0D, 0x09, 0x0E}
    };
} // namespace AES
