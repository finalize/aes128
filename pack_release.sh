#! /bin/bash
# 1. 确定包名
package_name=$1
iskeepDir=$2

if [ -z $package_name ]; then
    echo "Please input a version code (for example \"2.2.0\")"
    read package_name
fi

package_name="z-aes128-v"$package_name

if [ -d $package_name ]; then
    echo "Package directory: $package_name already exists, operation abort!"
    exit -1
fi

# 2. 拷贝库文件, 头文件和readme
mkdir $package_name
mkdir $package_name/include
cp build/libAES.dll $package_name
cp build/libAES.dll.a $package_name
cp AES/EnDecrypt.h $package_name/include
cp readme.md $package_name

# 3. 打成zip包
zip -r9 $package_name".zip" $package_name

# 4. 清理工作目录
if [ ! $iskeepDir ]; then
    rm -r $package_name
fi
